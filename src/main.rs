#![feature(proc_macro_hygiene, decl_macro, custom_attribute)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

use tiamo_back_auth::db::DatabaseConn;

#[get("/")]
fn index() -> &'static str {
    "TIAMO Auth System"
}

fn main() {
    rocket::ignite()
        .attach(DatabaseConn::fairing())
        .mount("/", routes![index])
        .launch();
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_it_works() {
        println!("Works!");
    }
}
