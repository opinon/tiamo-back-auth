use crate::schema::{friend_with, token, user};

/// Représente un utilisateur dans la base de données
#[derive(Identifiable, Queryable, PartialEq, Debug)]
#[table_name = "user"]
pub struct User {
    /// Identifiant de l'utilisateur
    pub id: i32,
    /// Identifiant du token unique associé à l'utilisateur
    pub id_token: Option<i32>,
    /// Date de création de l'utilisateur
    pub date_creation: std::time::SystemTime,
    /// Libelle de l'utilisateur (son pseudo)
    pub libelle_user: String,
}

#[derive(Insertable, PartialEq, Debug)]
#[table_name = "token"]
/// Représente un Token utilisateur dans la base de données
pub struct Token {
    /// Identifiant du Token
    pub id_token: i32,
    /// Date de création du token
    pub date_creation: std::time::SystemTime,
    /// Validité en jour du token (avant expiration)
    pub validity_period_days: i32,
}

#[derive(Insertable, PartialEq, Debug)]
#[table_name = "friend_with"]
/// Représente un lien d'amitié (bi-directionnel) entre deux
/// utilisateurs
pub struct Friendship {
    /// Utilisateur 1
    id_user_1: i32,
    /// Utilisateur 2
    id_user_2: i32,
    /// Date à laquelle l'amitié a été déclarée
    date_association: std::time::SystemTime,
}
