#![feature(proc_macro_hygiene, decl_macro, custom_attribute)]

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate rocket;

#[macro_use]
extern crate rocket_contrib;

pub mod db;
pub mod models;
pub mod schema;
