use rocket_contrib::databases::diesel;

#[database("tiamo_db")]
pub struct DatabaseConn(diesel::SqliteConnection);
