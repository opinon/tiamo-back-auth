-- Table : user
CREATE TABLE user (
    id_user       INTEGER  PRIMARY KEY AUTOINCREMENT
                           NOT NULL,
    id_token      INTEGER  CONSTRAINT fk_id_token_user_token REFERENCES token (id_token),
    date_creation DATETIME NOT NULL,
    libelle_user  TEXT     NOT NULL,
    mdp_user      TEXT     NOT NULL
);


-- Table : token
CREATE TABLE token (
    id_token             INTEGER  PRIMARY KEY AUTOINCREMENT
                                  NOT NULL,
    date_creation        DATETIME NOT NULL,
    validity_period_days INTEGER  NOT NULL
);

-- Table : friend_with
CREATE TABLE friend_with (
    id_user_1        INTEGER  CONSTRAINT fk_id_user_1_friend_with_user REFERENCES user (id_user) 
                              NOT NULL,
    id_user_2        INTEGER  CONSTRAINT fk_id_user_2_friend_with_user REFERENCES user (id_user) 
                              NOT NULL,
    date_association DATETIME NOT NULL,
    PRIMARY KEY (id_user_1,id_user_2)
);
