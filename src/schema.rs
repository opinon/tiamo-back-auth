table! {
    friend_with (id_user_1, id_user_2) {
        id_user_1 -> Integer,
        id_user_2 -> Integer,
        date_association -> Timestamp,
    }
}

table! {
    token (id_token) {
        id_token -> Integer,
        date_creation -> Timestamp,
        validity_period_days -> Integer,
    }
}

table! {
    user (id_user) {
        id_user -> Integer,
        id_token -> Nullable<Integer>,
        date_creation -> Timestamp,
        libelle_user -> Text,
        mdp_user -> Text,
    }
}

joinable!(user -> token (id_token));

allow_tables_to_appear_in_same_query!(
    friend_with,
    token,
    user,
);
